# M2 ANDROIDE MOSIMA PROJECT - 2016

## Goal

The goal of this project is to implement in netlogo [A multi-agent simulation platform for modeling perfectly rational and bounded-rational agents in organizations](http://jasss.soc.surrey.ac.uk/5/2/3.html) by Arianna Dal Forno and Ugo Merlone (2002), and reproduce the results.

This repository contains the project report (in French) and the netlogo source code.

## Simulation preview

Examples

### Affinity simulation

![agents_work_affinity](https://gitlab.com/phlf/netlogo_sma_project/raw/master/work_affinity_sim.png)

Groups formation due to the work affinity taken into account by agents


### Noise influence

![work_interaction_noise](https://gitlab.com/phlf/netlogo_sma_project/raw/master/interaction_noise.png)

Heatmap showing noise preventing agents to evaluate correctly their partner's effort and profit